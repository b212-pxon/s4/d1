package com.zuitt.batch212;

public class Car {
// Class creation
// A class is composed of four parts:
// 1. Property - characteristics of the object

    // Access modifier - Default, public, private, protected
    private String name;
    private String brand;
    private int manufactureDate;
    private String owner;

    // make driver a component of car
    private Driver d;

// 2. Constructors - used to create an object
// empty constructors
    public Car(){
        this.d = new Driver("Alejandro");
    }

// Parameterized constructor - accepts properties parameters
    public Car(String name, String brand, int manufactureDate, String owner) {
        this.name = name;
        this.brand = brand;
        this.manufactureDate = manufactureDate;
        this.owner = owner;
    }

// 3. getters and setters - get and set the values of each property of the object

// getters - read only or just to retrieve the properties

    //getters for driver
    public String getDriverName(){
        return this.d.getName();
    }

    public String getName(){
        return this.name;
    }

    public String getBrand(){
        return this.brand;
    }

    public int getManufactureDate(){
        return this.manufactureDate;
    }

    public String getOwner(){
        return this.owner;
    }

// setters - write only or adding up the properties of object
    public void setName(String name){
        this.name = name;
    }

    public void setBrand(String brand){
        this.brand = brand;
    }

    public void setManufactureDate(int manufactureDate){
        this.manufactureDate = manufactureDate;
    }

    public void setOwner(String owner){
        this.owner = owner;
    }

// 4. methods - function an object can perform
    public void drive(){
        System.out.println("The car is running.");
    }

    public void printDetails(){
        System.out.println("This " + getManufactureDate() + " " + getBrand() + " " + getName() + " is owned by " + getOwner());
    }


}
