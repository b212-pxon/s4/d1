package com.zuitt.batch212;

public interface Actions {

    public void sleep();

    public void run();

}
