package com.zuitt.batch212;

public class Animal {

    private String name;
    private String color;

    // Constructors
    public Animal(){}

    public Animal(String name, String color){
        this.name = name;
        this.color = color;
    }

    // getters and setters
    public String getName(){
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor(){
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    // method
    public void call(){
        System.out.println("Hi my name is " + this.name);
    }
}
