package com.zuitt.batch212;

public class StaticPoly {

    public int addition(int a, int b){
        return a + b;
    }

    // overload by changing the number of arguments
    public int addition(int a, int b, int c){
        return a + b + c;
    }

    // overload by changing the type of arguments
    public double addition(double a, double b){
        return a + b;
    }

    // method overloading: when there are multiple method with the same name but different parameters (by number, by type)

}
