package com.zuitt.batch212;

public class Main {

    public static void main(String[] args) {

        // Instantiation of object (new keyword to create an object)

        Car myCar = new Car();
        System.out.println(myCar);
        myCar.setName("Civic");
        myCar.setBrand("Honda");
        myCar.setManufactureDate(1998);
        myCar.setOwner("John");

        System.out.println(myCar.getName());
        System.out.println(myCar.getBrand());

        // instantiate using the parameterized constructor
        Car otherCar = new Car("Charger", "Dodge", 1978, "Vin Diesel");

        System.out.println(otherCar.getName());
        System.out.println(myCar.getName());

        // method
        myCar.drive();
        otherCar.drive();

        myCar.printDetails();
        otherCar.printDetails();

        // Encapsulation is a mechanism of wrapping the data (variables) and code acting on the data (methods) together as a single unit

        // to achieve encapsulations
        // Declare the variables of a class as private
        // provide public setter and getter methods to modify and view the variables values

        Car newCar = new Car();

        newCar.setName("Lightning Mcqueen");
        System.out.println(newCar.getName());
        System.out.println("This car is driven by " + newCar.getDriverName());

        //newCar.name = "new Name";

        // Inheritance = allows modelling objects that is a subset of another objects. It is defined "is a relationship"

        // Inheritance

        Dog dog1 = new Dog();

        dog1.setName("Brownie");
        System.out.println(dog1.getName());

        dog1.setColor("Brown");
        dog1.setBreed("Aspin");

        System.out.println(dog1.getColor());
        System.out.println(dog1.getBreed());

        dog1.call();
        dog1.speak();

        // composition - allows modelling objects that are made up of each other objects.
        // it defines a "has a relationship"

        // A car has a driver - composition
        // A car is a vehicle - inheritance

        //Abstraction - is the process of hiding certain details and showing only essential information to the user.

        // Interfaces are used ro achieve total abstraction


        // Abstraction using inheritance (implements)
        Person jane = new Person();

        jane.sleep();
        jane.run();
        jane.tall();

        // Polymorphism - the ability of an object to take on many forms. It can have a same method name, but different functions. or can have different forms

        // Two types of polymorphism
        // 1. Static or compile-time polymorphism - this is usually done by method OVERLOADING. methods can be overloaded by change in no. or arguments or change in type of arguments

        StaticPoly print = new StaticPoly();

        System.out.println(print.addition(5,5));
        System.out.println(print.addition(5,5,5));
        System.out.println(print.addition(86.5, 90.5));

        // 2. Dynamic or run-time polymorphism - this is usually done by method OVERRIDING
        // methods overridden by replacing the definition of the method in the parent to the children classes.

        // Runtime polymorphism

        Parent parent1 = new Parent();
        parent1.speak(); //parent class

        Parent subObject = new Child();
        subObject.speak();
        // method of subclass or child class is called by parent reference. This is called run time polymorphism



    }


}
