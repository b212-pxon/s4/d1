package com.zuitt.batch212;

public class Dog extends Animal {

    // extends - is the keyword used to inherit the properties of a class
    // super can be used to refer immediate parent class method
    // used to invoke immediate parent class method

    // properties
    private String breed;

    // constructors
    public Dog(){
        // super - keyword in java as a reference variable which is used to refer immediate parent class object
        super(); // empty constructor of Animal()

    }

    public Dog(String name, String color, String breed){
        super(name, color); // Animal - name, color
        this.breed = breed;
    }

    public String getBreed(){
        return this.breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public void speak(){
        super.call();
        System.out.println("Bark");
    }

}
