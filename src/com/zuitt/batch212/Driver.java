package com.zuitt.batch212;

public class Driver {

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public Driver(){}

    public Driver(String name){
        this.name = name;
    }
}
