package com.zuitt.batch212;

public class Person implements Actions, Characteristic {

    public void sleep() {
        System.out.println("Zzz.....");
    }

    public void run() {
        System.out.println("Running");
    }

    public void tall() {
        System.out.println("7 foot");
    }
}
